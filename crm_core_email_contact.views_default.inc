<?php

/**
 * @file
 * Change default configuration for CRM Core Contact view by adding new action for VBO.
 */

function crm_core_email_contact_views_default_views_alter(&$views) {
  if (isset($views['crm_core_contacts']) &&
      !empty($views['crm_core_contacts']->display['default']->display_options['fields']['views_bulk_operations']['vbo']['operations'])) {
    $views['crm_core_contacts']->display['default']->display_options['fields']['views_bulk_operations']['vbo']['operations']['rules_component::rules_send_mail_to_crm_contact'] = array(
      'selected' => 1,
      'use_queue' => 0,
      'skip_confirmation' => 1,
      'override_label' => 1,
      'label' => 'Send mail to contact',
    );
  }
}