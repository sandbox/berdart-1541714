<?php

/**
 * @file
 * Default Rules configuration for CRM Core Email Contact
 */

/**
 * Implements hook_default_rules_configuration().
 */
function crm_core_email_contact_default_rules_configuration() {
  $rules = array();

  $rule_config = <<<RULE
{ "rules_send_mail_to_crm_contact" : {
    "LABEL" : "Send mail to CRM Contact",
    "PLUGIN" : "action set",
    "REQUIRES" : [ "crm_core_email_contact" ],
    "USES VARIABLES" : {
      "contact" : { "label" : "Contact", "type" : "crm_core_contact" },
      "subject" : { "label" : "Subject", "type" : "text" },
      "from" : { "label" : "From", "type" : "text" },
      "to" : { "label" : "To", "type" : "text" },
      "message" : { "label" : "Message", "type" : "text_formatted" }
    },
    "ACTION SET" : [
      { "filtered_mail" : {
          "to" : [ "to" ],
          "subject" : [ "subject" ],
          "message" : [ "message" ],
          "from" : [ "from" ],
          "language" : [ "" ]
        }
      }
    ]
  }
}
RULE;

  $rules['rules_send_mail_to_crm_contact'] = rules_import($rule_config);

  return $rules;
}
